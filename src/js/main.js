import './vendor';
import './partials/fixedHeader';
import './partials/testimonialsSlider';
import './partials/hamburgerMenu';
import './partials/modals';
