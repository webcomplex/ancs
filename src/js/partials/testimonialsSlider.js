let $testimonialsSlider = $('.js-testimonials-slider');
let $testimonialsSliderControls = $('.js-testimonials-slider-controls');
let testimonialsSliderItemClass = '.js-testimonials-slider-item';

$testimonialsSlider.slick({
	arrows: true,
	dots: true,
	adaptiveHeight: true,
	infinite: true,
	autoplay: true,
	autoplaySpeed: 3000,
	speed: 400,
	slidesToShow: 1,
	slidesToScroll: 1,
	nextArrow: '<i class="sprite-arrow-next slick-next"></i>',
	prevArrow: '<i class="sprite-arrow-prev slick-prev"></i>',
	appendDots: $testimonialsSliderControls,
	appendArrows: $testimonialsSliderControls,
	customPaging: (slider, i) => {
		let thumb = $(slider.$slides[i]).find(testimonialsSliderItemClass).data('thumb');

		return `<div class="slick-dots__item-inner"><img src="${thumb}" alt=""></div>`;
	},
});
