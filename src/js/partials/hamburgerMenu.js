let $siteHeader = $('.js-header');
let $hamburger = $('.js-hamburger-menu');
let $menu = $('.js-mobile-nav');

$hamburger.on('click', (event) => {
	let $that = $(event.currentTarget);

	$that.toggleClass('is-active');
	$menu.toggleClass('is-active');
	$siteHeader.toggleClass('is-mobile-opened');
});
