let $modalVideoBtn = $('.js-modal-video');
let $modal = $('#modal-iframe');

$modal.iziModal({
	iframe: true,
	iframeHeight: 320,
	width: 568,
	fullscreen: true,
	headerColor: '#000000',
});

$modalVideoBtn.on('click', (event) => {
	event.preventDefault();

	$modal.iziModal('open');
});
