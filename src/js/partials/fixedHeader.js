let $siteHeader = $('.js-header');
let $window = $(window);
let scrollLimit = 20;

$window.on('scroll', (event) => {
	let scrollValue = $(event.currentTarget).scrollTop();

	if (scrollValue >= scrollLimit) {
		$siteHeader.addClass('is-active');
	} else {
		$siteHeader.removeClass('is-active');
	}
});
