import 'babel-polyfill';
import svg4everybody from 'svg4everybody';
import $ from 'jquery';
import 'slick-carousel';
import iziModal from 'izimodal';

svg4everybody();

window.$ = $;
window.jQuery = $;

$.fn.iziModal = iziModal;

require('ninelines-ua-parser');
